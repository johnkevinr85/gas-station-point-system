class CreateGas < ActiveRecord::Migration[5.1]
  def change
    create_table :gas do |t|
      t.string :gas_type
      t.decimal :price, precision: 10, scale: 2

      t.timestamps
    end
  end
end
