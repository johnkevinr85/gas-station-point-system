class CreateGasVolumes < ActiveRecord::Migration[5.1]
  def change
    create_table :gas_volumes do |t|
      t.integer :gas_id
      t.integer :total_volume
      t.integer :remaining_volume

      t.timestamps
    end
  end
end
