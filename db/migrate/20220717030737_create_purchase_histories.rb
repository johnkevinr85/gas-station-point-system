class CreatePurchaseHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :purchase_histories do |t|
      t.integer :buyer_id
      t.integer :gas_volume_id
      t.decimal :total_pay, precision: 10, scale: 2
      t.decimal :total_discount, precision: 10, scale: 2
      t.decimal :liters_bought, precision: 10, scale: 2
      t.integer :with_loyalty, default: 0

      t.timestamps
    end
  end
end
