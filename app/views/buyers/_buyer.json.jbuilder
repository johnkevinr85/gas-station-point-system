json.extract! buyer, :id, :name, :loyalty_card, :created_at, :updated_at
json.url buyer_url(buyer, format: :json)
