class GasController < ApplicationController
  before_action :set_gas, only: %i[ show edit update destroy ]
  before_action :get_purchase_history, only: %i[ dashboard ]
  before_action :set_type, only: %i[ reports ]

  # GET /gas or /gas.json
  def index
    @gas = Gas.select('gas.*, gas_volumes.remaining_volume').joins(:gas_volumes).all || []
  end

  def dashboard
    @gas = Gas.select('gas.*, gas_volumes.remaining_volume, gas_volumes.id as volume_id').joins(:gas_volumes).where.not(gas_volumes: {remaining_volume: 0}).group(:gas_id).all || []
    @buyers = Buyer.all || []
    @last_buyer = PurchaseHistory.last.buyer_id
  end

  # GET /gas/1 or /gas/1.json
  def show
  end

  # GET /gas/new
  def new
    @gas = Gas.new
  end

  # GET /gas/1/edit
  def edit
  end

  # POST /gas or /gas.json
  def create
    @gas = Gas.new(gas_params)
    remaining_volume = params[:gas][:remaining_volume].present? ? params[:gas][:remaining_volume] : GasVolume::INITIAL_VOLUME
    @gas_volume = @gas.gas_volumes.build(total_volume: GasVolume::GAS_TANK_CAPACITY, remaining_volume: remaining_volume)

    respond_to do |format|
      if @gas.save
        format.html { redirect_to gas_url(@gas), notice: "Gas was successfully created." }
        format.json { render :show, status: :created, location: @gas }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @gas.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gas/1 or /gas/1.json
  def update
    respond_to do |format|
      if @gas.update(gas_params)
        remaining_volume = params[:gas][:remaining_volume].present? ? params[:gas][:remaining_volume] : GasVolume::INITIAL_VOLUME

        if @gas.gas_volumes.last.remaining_volume > 0
          @gas.gas_volumes.last.update_attributes(remaining_volume: remaining_volume)
        else
          @gas_volume = @gas.gas_volumes.build(total_volume: GasVolume::GAS_TANK_CAPACITY, remaining_volume: remaining_volume)
          @gas_volume.save
        end
        
        format.html { redirect_to gas_url(@gas), notice: "Gas was successfully updated." }
        format.json { render :show, status: :ok, location: @gas }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @gas.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gas/1 or /gas/1.json
  def destroy
    @gas.destroy

    respond_to do |format|
      format.html { redirect_to gas_index_url, notice: "Gas was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def purchase
    gas_ids = params[:gas]&.keys

    @gas_volumes = GasVolume.where(id: gas_ids).where.not(remaining_volume: 0).group(:gas_id)

    respond_to do |format|
      @gas_volumes.each_with_index do |gas, index|
        remaining_volume = gas.remaining_volume
        volume_to_purchase = params[:gas].values[index].to_i

        @purchase_history = PurchaseHistory.new

          if volume_to_purchase.to_i > 0
              if remaining_volume >= volume_to_purchase
                gas.update_attributes!(remaining_volume: remaining_volume - volume_to_purchase)

                @purchase_history.gas_volume = gas
                @purchase_history.buyer_id = params[:buyer_id]
                @purchase_history.total_pay = params[:total_gas_pay]
                @purchase_history.total_discount = params[:total_gas_discount]
                @purchase_history.liters_bought = params[:gas].values[index]
                @purchase_history.with_loyalty = 1 if params[:loyalty_card].present?
                @purchase_history.save!

                format.html { redirect_to root_path, notice: "Gas purchased successfully." }
                format.json { render :dashboard, status: :created }
              else
                gas = GasVolume.select('gas.gas_type').joins(:gas).where(params[:gas].keys[index]).first
                format.html { redirect_to root_path, notice: "Insufficient Purchase. Remaining Volume for #{gas.gas_type} is #{remaining_volume} Liters" }
                format.json { head :no_content }
              end
          end
       end 
    else
      format.html { redirect_to root_path, notice: "Invalid Purchase." }
      format.json { head :no_content }
    end
  end

  def reports
    if @type == "complete_purchase"
      get_purchase_history
    else
      @purchase_histories = PurchaseHistory.select('*').joins(:gas_volume, :buyer, :gas).where.not(with_loyalty: 0).order(id: :desc) || []
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gas
      @gas = Gas.select('gas.*, gas_volumes.remaining_volume').joins(:gas_volumes).where(gas: {id: params[:id]}).order("gas_volumes.remaining_volume desc").first || []
    end

    def get_purchase_history
      @purchase_histories = PurchaseHistory.select('*').joins(:gas_volume, :buyer, :gas).order(id: :desc) || []
    end

    def set_type
      @type = if params[:type] != "complete_purchase"
        "loyalty_buyer"
      else
        "complete_purchase"
      end
    end
    # Only allow a list of trusted parameters through.
    def gas_params
      params.require(:gas).permit(:gas_type, :price, :price)
    end
end
