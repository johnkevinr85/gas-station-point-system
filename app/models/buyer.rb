class Buyer < ActiveRecord::Base
	has_many :purchase_histories, :dependent => :destroy

	validates_presence_of :name
	
end
