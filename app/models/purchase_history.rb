class PurchaseHistory < ActiveRecord::Base
	belongs_to :gas_volume
	belongs_to :buyer
	has_one :gas, :through => :gas_volume

	validates_presence_of :buyer
	validates_presence_of :gas_volume
	validates_presence_of :total_pay
	validates_presence_of :liters_bought
end
