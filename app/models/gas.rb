class Gas < ActiveRecord::Base
	has_many :gas_volumes, :dependent => :destroy

	validates_presence_of :gas_type
	validates :price, numericality: { less_than_or_equal_to: 999999, greater_than_or_equal_to: 0 }

	LOYALTY_CARD = ["SM Advantage", "Robinsons Rewards"]

	def self.loyalty_cards
		LOYALTY_CARD
	end
end
