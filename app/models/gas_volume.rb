class GasVolume < ActiveRecord::Base
	belongs_to :gas
	has_many :purchase_histories, :dependent => :destroy

	validates_presence_of :gas
	validates_presence_of :total_volume

	INITIAL_VOLUME = 0
	# 100 liters gas tank capacity
	GAS_TANK_CAPACITY = 100
end
