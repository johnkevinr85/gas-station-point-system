module ApplicationHelper

	def remaining_gas_percentage(remaining_volume)
		(remaining_volume.to_f / GasVolume::GAS_TANK_CAPACITY) * 100
	end

	def total_gas_remaining(remaining_volumes)
		tank_used = remaining_volumes.count
		((remaining_volumes.inject(0, :+).to_f * 100) / (tank_used * 100)).round(2) || 0
	end

	def reports_helper(type = "complete_purchase")
		type == "complete_purchase" ? "Complete Purchase History" : "Loyalty Buyer Transcation History"
	end
end
