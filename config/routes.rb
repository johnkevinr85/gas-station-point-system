Rails.application.routes.draw do
  resources :buyers
  resources :gas

  post '/purchase_gas' => 'gas#purchase'
  get '/reports' => 'gas#reports'

  root to: "gas#dashboard"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
