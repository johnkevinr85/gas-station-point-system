require 'test_helper'

class GasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gas = gas(:one)
  end

  test "should get index" do
    get gas_index_url
    assert_response :success
  end

  test "should get new" do
    get new_gas_url
    assert_response :success
  end

  test "should create gas" do
    assert_difference('Gas.count') do
      post gas_index_url, params: { gas: { gas_type: @gas.gas_type, price: @gas.price, price: @gas.price } }
    end

    assert_redirected_to gas_url(Gas.last)
  end

  test "should show gas" do
    get gas_url(@gas)
    assert_response :success
  end

  test "should get edit" do
    get edit_gas_url(@gas)
    assert_response :success
  end

  test "should update gas" do
    patch gas_url(@gas), params: { gas: { gas_type: @gas.gas_type, price: @gas.price, price: @gas.price } }
    assert_redirected_to gas_url(@gas)
  end

  test "should destroy gas" do
    assert_difference('Gas.count', -1) do
      delete gas_url(@gas)
    end

    assert_redirected_to gas_index_url
  end
end
